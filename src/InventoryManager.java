import java.util.HashMap;
import java.util.Map;

public class InventoryManager {
    private Map<String, Integer> inventory;

    public InventoryManager() {
        this.inventory = new HashMap<>();
    }

    public void addItem(String item, int quantity) {
        inventory.put(item, quantity);
    }

    public void updateQuantity(String item, int quantity) {
        if (inventory.containsKey(item)) {
            inventory.put(item, quantity);
        } else {
            System.out.println(item + " does not exist in the inventory.");
        }
    }

    public void removeItem(String item) {
        inventory.remove(item);
    }

    public boolean itemExists(String item) {
        return inventory.containsKey(item);
    }

    public void printInventory() {
        System.out.println("Inventory:");
        for (Map.Entry<String, Integer> entry : inventory.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}