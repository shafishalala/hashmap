public class Main {
    public static void main(String[] args) {
        InventoryManager manager = new InventoryManager();

        manager.addItem("Apple", 10);
        manager.addItem("Banana", 15);
        manager.addItem("Orange", 20);

        manager.updateQuantity("Banana", 25);

        manager.removeItem("Orange");

        System.out.println("Item 'Apple' exists: " + manager.itemExists("Apple"));
        System.out.println("Item 'Orange' exists: " + manager.itemExists("Orange"));

        manager.printInventory();
    }
}